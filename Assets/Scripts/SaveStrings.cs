public static class SavesStrings
{
    public static string Music = "music";
    public static string EducationObject = "educationObject";

    #region Education Objects

    public static string MoonObj = "Moon";
    public static string MoonRoverObj = "MoonRover";

    #endregion
}