using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AppManager : MonoBehaviour
{
    //todo: в сцене луны от этого скрипта избавился. В будущем уберем и из лунохода
    public TextMeshProUGUI  message;
    void Update()
    {
        if (Input.touchCount > 0)
        {
            message.gameObject.SetActive(false);
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                var ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                if (Physics.Raycast(ray))
                {
                    SceneManager.LoadScene(1);
                }

            }
        }
        else
        {
            message.gameObject.SetActive(true);
        }
    }
}
