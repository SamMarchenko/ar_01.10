using System;
using AssetManagement;
using ModestTree;
using TemporaryScripts;
using UnityEngine;

namespace Factories
{
    public class ArObjectFactory: IArObjectFactory
    {
        private readonly IAssetProvider _assetProvider;

        public ArObjectFactory(IAssetProvider assetProvider)
        {
            _assetProvider = assetProvider;
        }

        public ArObjectView CreateArObj(EducationObject educationObject)
        {
            var path = FindAssetPath(educationObject);

            if (path.IsEmpty())
            {
                Debug.LogException(new Exception("Path of this Object not found"));
                return null;
            }

            return _assetProvider.Instantiate(path).GetComponent<ArObjectView>();
        }

        public string FindAssetPath(EducationObject educationObject)
        {
            string path = string.Empty;
            switch (educationObject)
            {
                case EducationObject.Moon:
                    path = AssetPath.MoonPath;
                    break;
                case EducationObject.MoonRover:
                    path = AssetPath.MoonRoverPath;
                    break;
            }

            return path;
        }
    }
}