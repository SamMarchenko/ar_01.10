using TemporaryScripts;

namespace Factories
{
    public interface IArObjectFactory
    {
        ArObjectView CreateArObj(EducationObject educationObject);
        string FindAssetPath(EducationObject educationObject);
    }
}