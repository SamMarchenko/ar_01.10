using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace TemporaryScripts
{
    public class MetaSceneView : MonoBehaviour
    {
        public List<ObjSceneBtnView> ObjSceneBtnViews = new List<ObjSceneBtnView>();


        [Serializable]
        public class ObjSceneBtnView
        {
            public Button Button;
            public EducationObject EducationObject;

            public void AddCallbackToLoadArSceneButton(UnityAction action)
            {
                Button.onClick.AddListener(action);
            }
        }
    }
}