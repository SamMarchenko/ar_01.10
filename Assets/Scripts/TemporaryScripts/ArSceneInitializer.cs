using System.Collections.Generic;
using System.Linq;
using Data;
using DefaultNamespace.Views;
using Factories;
using Services.Core;
using UnityEngine;
using Zenject;

namespace TemporaryScripts
{
    public class ArSceneInitializer : IInitializable, ITickable, ISceneDataFinder
    {
        private readonly List<INeedSceneData> _needSceneDataList;
        private readonly List<IStartable> _startables;
        private readonly List<IUpdatable> _updatables;
        private readonly IArObjectFactory _arObjectFactory;
        private readonly ArObjectParent _arObjectParent;
        private readonly ScenesSettingsPreset _settingsPreset;
        private SceneData _sceneData;
        private EducationObject _educationObjectType;

        public ArSceneInitializer(
            List<INeedSceneData> needSceneDataList,
            List<IStartable> startables,
            List<IUpdatable> updatables,
            IArObjectFactory arObjectFactory,
            ArObjectParent arObjectParent,
            ScenesSettingsPreset settingsPreset)
        {
            _needSceneDataList = needSceneDataList;
            _startables = startables;
            _updatables = updatables;
            _arObjectFactory = arObjectFactory;
            _arObjectParent = arObjectParent;
            _settingsPreset = settingsPreset;
        }

        public void Initialize()
        {
            _educationObjectType = FindEduObjectType();
            CreateEduArObject();
            FindSceneData(_settingsPreset);
            SetSceneData();
            StartAllClasses();
        }

        public void Tick()
        {
            foreach (var updatable in _updatables)
            {
                updatable.Update();
            }
        }

        public void FindSceneData(ScenesSettingsPreset settingsPreset)
        {
            var data = settingsPreset.SceneDatas.FirstOrDefault(o =>
                o.EducationObject == _educationObjectType);
            if (data == null)
            {
                Debug.LogError("Нет данных для текущей сцены. Проверьте ScenesSettingsPreset");
                return;
            }

            _sceneData = data;
        }

        private void SetSceneData()
        {
            foreach (var e in _needSceneDataList)
            {
                e.SetSceneData(_sceneData);
            }
        }

        private void StartAllClasses()
        {
            foreach (var startable in _startables)
            {
                startable.Start();
            }
        }

        private void CreateEduArObject()
        {
            var eduObj = _arObjectFactory.CreateArObj(_educationObjectType);
            eduObj.transform.parent = _arObjectParent.transform;
            _arObjectParent.Init(eduObj);
        }

        private EducationObject FindEduObjectType()
        {
            var eduObjectName = PlayerPrefs.GetString(SavesStrings.EducationObject);

            if (eduObjectName == SavesStrings.MoonObj)
            {
                return EducationObject.Moon;
            }

            if (eduObjectName == SavesStrings.MoonRoverObj)
            {
                return EducationObject.MoonRover;
            }

            Debug.LogError("Edu Object type with such name NOT FOUND");
            return EducationObject.None;
        }
        
    }
}