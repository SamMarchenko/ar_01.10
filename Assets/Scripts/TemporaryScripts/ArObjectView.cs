using UnityEngine;
using UnityEngine.EventSystems;

namespace TemporaryScripts
{
    public class ArObjectView : MonoBehaviour, IPointerClickHandler
    {
        public EducationObject EducationObject;
        public EducationObject OnClickLoadNextObject;
        public float MinScale = 0.5f;
        public float MaxScale = 2f;
        public float PosYOffset = -0.05f;
        public Collider Collider;
        public void OnPointerClick(PointerEventData eventData)
        {
            
        }
    }
}