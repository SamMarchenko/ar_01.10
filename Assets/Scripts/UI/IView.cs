namespace UI
{
    public interface IView
    {
        public void Show();
        public void Hide();
    }
}