using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class TopPanelView : MonoBehaviour, IView
    {
        [SerializeField] private Button _backBtn;
        [SerializeField] private Button _closeBtn;

        public Button BackBtn => _backBtn;
        public Button CloseBtn => _closeBtn;
        public void Show() => 
            gameObject.SetActive(true);

        public void Hide() => 
            gameObject.SetActive(false);
    }
}