using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class QuestionsWindowView : MonoBehaviour, IView
    {
        [SerializeField] private FinishTestWindowView _finishTestWindowView;
        [SerializeField] private TextMeshProUGUI _questionText;
        [SerializeField] private Transform _firstStringVariantsHolder;
        [SerializeField] private Transform _secondStringVariantsHolder;
        [SerializeField] private NextQuestionBtnView _nextQuestionBtnView;
        [SerializeField] private List<VariantBtnView> _variantBtns;
        [SerializeField] private Sprite[] _answerSprites = new Sprite[3];
        [SerializeField] private Button _backBtn;


        public Button BackBtn => _backBtn;
        public FinishTestWindowView FinishTestWindowView => _finishTestWindowView;
        public TextMeshProUGUI QuestionText => _questionText;
        public List<VariantBtnView> VariantsBtns => _variantBtns;
        public NextQuestionBtnView NextQuestionButtonView => _nextQuestionBtnView;

        public Sprite[] AnswerSprites => _answerSprites;

        public void ResetAllBtnsFontSize()
        {
            foreach (var variantBtnView in _variantBtns) 
                variantBtnView.BtnText.fontSizeMax = 100;
        }

        public void Show() => 
            gameObject.SetActive(true);

        public void Hide() => 
            gameObject.SetActive(false);


        [Serializable]
        public class VariantBtnView
        {
            public Button Btn;
            public TextMeshProUGUI BtnText;
            public Image BtnImage;
            public bool IsRightAnswer;
        }

        [Serializable]
        public class NextQuestionBtnView
        {
            public Button Btn;
            public TextMeshProUGUI BtnText;
            public Color DisableBtnTextColor;
            public Image BtnImage;
            public Sprite[] BtnViewSprites;
        }
    }
}