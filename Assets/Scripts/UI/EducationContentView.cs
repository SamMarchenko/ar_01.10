using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UI
{
    public class EducationContentView : MonoBehaviour, IView
    {
        [SerializeField] private TopPanelView _topPanelView;
        [SerializeField] private TextMeshProUGUI _chapterText;
        [SerializeField] private TextMeshProUGUI _headerText;
        [SerializeField] private TextMeshProUGUI _educationText;
        [SerializeField] private Image _backgroundImage;
        [SerializeField] private Button _eduTestStartBtn;
        [SerializeField] private Button _speakerBtn;
        [SerializeField] private ScrollRect _contentRect;

        public Button StartTestBtn => _eduTestStartBtn;
        public Button SpeakerBtn => _speakerBtn;

        public void SetBackgroundImage(Sprite sprite) => 
            _backgroundImage.sprite = sprite;

        public void SetAllText(string chapter, string header, string eduText)
        {
            _chapterText.text = chapter;
            _headerText.text = header;
            _educationText.text = eduText;
        }

        public void AddCallbackToBackButton(UnityAction action) => 
            _topPanelView.BackBtn.onClick.AddListener(action);

        private void OnDestroy() => 
            Unsubscribe();

        private void Unsubscribe() => 
            _topPanelView.BackBtn.onClick.RemoveAllListeners();

        public void Show() => 
            gameObject.SetActive(true);

        public void Hide()
        {
            _contentRect.normalizedPosition = new Vector2(0f, 1f);
            gameObject.SetActive(false);
        }
    }
}
