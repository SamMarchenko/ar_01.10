using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class FinishTestWindowView : MonoBehaviour, IView
    {
        [SerializeField] private TextMeshProUGUI _resultText;
        [SerializeField] private Button _mainMenuBtn;
        [SerializeField] private Button _modelSceneBtn;
        [SerializeField, Range(0.5f, 3f)] private float _delayBeforeShow;

        public TextMeshProUGUI ResultTex => _resultText;
        public Button MainMenuBtn => _mainMenuBtn;
        public Button ModelSceneBtn => _modelSceneBtn;
        public float DelayBeforeShow => _delayBeforeShow;

        public void Show() => 
            gameObject.SetActive(true);

        public void Hide() => 
            gameObject.SetActive(false);
    }
}