using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class MainUIView : MonoBehaviour, IView
    {
        [SerializeField] private TextMeshProUGUI _tutorialMsgText;
        [SerializeField] private Button _educationButton;
        [SerializeField] private Button _screenShotButton;
        [SerializeField] private Button _soundButton;
        [SerializeField] private List<SoundButtonView> _soundButtonViews;
        
        public Button EducationBtn => _educationButton;
        public Button ScreenshotBtn => _screenShotButton;
        public Button SoundBtn => _soundButton;

        public List<SoundButtonView> SoundButtonViews => _soundButtonViews;
        public TextMeshProUGUI TutorialMsgText => _tutorialMsgText;
       

        [Serializable]
        public class SoundButtonView
        {
            public Sprite ViewSprite;
            public bool IsSoundOn;
        }

        public void Show() => 
            gameObject.SetActive(true);

        public void Hide() => 
            gameObject.SetActive(false);
    }
}