using UI;
using UnityEngine;

//todo: в сцене луны от этого скрипта избавился. В будущем уберем и из лунохода
public class InformationComponent : MonoBehaviour
{
    [SerializeField]
    private EducationContentView educationContentView;

    public void ShowInfo()
    {
        educationContentView.gameObject.SetActive(true);
    }

    public void CloseInfo()
    {
        educationContentView.gameObject.SetActive(false);
    }
}