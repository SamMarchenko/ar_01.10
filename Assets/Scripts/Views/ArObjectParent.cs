using Lean.Touch;
using TemporaryScripts;
using UnityEngine;

namespace DefaultNamespace.Views
{
    public class ArObjectParent : MonoBehaviour
    {
        [SerializeField] private ArObjectView _arObjectView;
        
        public LeanPinchScale LeanPinchScale; 
        //public Collider Collider;

        public void Init(ArObjectView arObjectView)
        {
            _arObjectView = arObjectView;
            //SetColliderToParent();
            LeanPinchScale.MinScale = _arObjectView.MinScale;
            LeanPinchScale.MaxScale = _arObjectView.MaxScale;
        }

        // private void SetColliderToParent()
        // {
        //     var type = ArObjectView.Collider.GetType();
        //     if (type == typeof(BoxCollider))
        //     {
        //         Collider = gameObject.AddComponent<BoxCollider>();
        //         var tempObjectCol = ArObjectView.Collider as BoxCollider;
        //         var tempParentCol = Collider as BoxCollider;
        //         tempParentCol.center = tempObjectCol.center + new Vector3(0, ArObjectView.PosYOffset, 0);
        //         tempParentCol.size = tempObjectCol.size;
        //         Collider = tempParentCol;
        //     }
        //     else if (type == typeof(SphereCollider))
        //     {
        //         Collider = gameObject.AddComponent<SphereCollider>();
        //         var tempObjectCol = ArObjectView.Collider as SphereCollider;
        //         var tempParentCol = Collider as SphereCollider;
        //         tempParentCol.center = tempObjectCol.center + new Vector3(0, ArObjectView.PosYOffset, 0);
        //         tempParentCol.radius = tempObjectCol.radius;
        //         Collider = tempParentCol;
        //     }
        //     else if (type == typeof(CapsuleCollider))
        //     {
        //         Collider = gameObject.AddComponent<CapsuleCollider>();
        //         var tempObjectCol = ArObjectView.Collider as CapsuleCollider;
        //         var tempParentCol = Collider as CapsuleCollider;
        //         tempParentCol.center = tempObjectCol.center + new Vector3(0, ArObjectView.PosYOffset, 0);
        //         tempParentCol.radius = tempObjectCol.radius;
        //         Collider = tempParentCol;
        //     }
        // }
    }
}