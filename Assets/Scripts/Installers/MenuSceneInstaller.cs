using Services.Meta;
using TemporaryScripts;
using UnityEngine;
using Zenject;

public class MenuSceneInstaller : MonoInstaller
{
    [SerializeField, Header("VIEW")] private MetaSceneView _metaSceneView;

    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<MetaSceneView>().FromInstance(_metaSceneView);
        Container.BindInterfacesAndSelfTo<MetaSceneService>().AsSingle().NonLazy();
    }
}