using AssetManagement;
using DefaultNamespace.Views;
using Factories;
using Services;
using Services.Core;
using Settings;
using TemporaryScripts;
using UI;
using UnityEngine;
using Zenject;

namespace Installers
{
    public class ArModelSceneInstaller : MonoInstaller
    {
        [SerializeField] private CoroutineRunService _coroutineRunService;
        [SerializeField] private AudioSource _audioSource;

        [SerializeField, Header("VIEW")] private MainUIView _mainUIView;
        [SerializeField] private EducationContentView _educationContentView;
        [SerializeField] private QuestionsWindowView _questionsWindowView;
        [SerializeField] private ArObjectParent _arObjectParent;


        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<ArObjectParent>().FromInstance(_arObjectParent);
            BindMainUI();
            BindEducationUI();

            Container.BindInterfacesAndSelfTo<AudioSource>().FromInstance(_audioSource);

            BindServices();
            BindFactories();
            Container.BindInterfacesAndSelfTo<AssetProvider>().AsSingle().NonLazy();

            Container.BindInterfacesAndSelfTo<ArSceneInitializer>().AsSingle().NonLazy();
        }

        private void BindFactories()
        {
            Container.BindInterfacesAndSelfTo<ArObjectFactory>().AsSingle().NonLazy();
        }

        private void BindServices()
        {
            Container.BindInterfacesAndSelfTo<CoroutineRunService>().FromInstance(_coroutineRunService);

            Container.BindInterfacesAndSelfTo<EducationService>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<ScreenshotService>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<MusicService>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<QuestionsTestService>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<MainModelSceneService>().AsSingle().NonLazy();
        }

        private void BindEducationUI()
        {
            Container.BindInterfacesAndSelfTo<EducationContentView>().FromInstance(_educationContentView);
            Container.BindInterfacesAndSelfTo<QuestionsWindowView>().FromInstance(_questionsWindowView);
        }

        private void BindMainUI() =>
            Container.BindInterfacesAndSelfTo<MainUIView>().FromInstance(_mainUIView);
    }
}