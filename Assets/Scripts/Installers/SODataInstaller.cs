using Data;
using UnityEngine;
using Zenject;

namespace Installers
{
    [CreateAssetMenu(fileName = "SOInstaller", menuName = "Installers/SOInstaller")]
    public class SODataInstaller : ScriptableObjectInstaller
    {
        [SerializeField] private ScenesSettingsPreset _scenesSettingsPreset;
        public override void InstallBindings()
        {
            Container.BindInstance(_scenesSettingsPreset);
        }
    
    }
}
