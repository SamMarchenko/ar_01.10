using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "NewSceneData", menuName = "Data/SceneData")]
    public class SceneData: ScriptableObject
    {
        [SerializeField] private EducationObject _educationObject;
        [SerializeField] private Sprite _eduBackgroundImageSprite;
        [SerializeField] private EduTestData _eduTestData;

        public EducationObject EducationObject => _educationObject;
        public Sprite EduBackgroundImageSprite => _eduBackgroundImageSprite;
        public EduTestData EduTestData => _eduTestData;
    }
}