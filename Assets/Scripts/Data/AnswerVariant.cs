using System;

namespace Data
{
    [Serializable]
    public class AnswerVariant
    {
        public string VariantText;
        public bool IsRightAnswer;
    }
}