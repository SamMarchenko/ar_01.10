using System.Collections.Generic;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "NewEduData", menuName = "Data/EducationData")]
    public class EduTestData: ScriptableObject
    {
        [SerializeField] private string _chapterText;
        [SerializeField] private string _headerText;
        [SerializeField, TextArea(minLines:1,maxLines:30)] private string _educationText;
        [SerializeField] private List<TestQuestion> _testQuestions;
        
        public string ChapterText => _chapterText;
        public string HeaderText => _headerText;
        public string EducationText => _educationText;
        public List<TestQuestion> TestQuestions => _testQuestions;
    }
}