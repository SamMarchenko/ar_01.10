using System;
using System.Collections.Generic;

namespace Data
{
    [Serializable]
    public class TestQuestion
    {
        public string QuestionText;
        public List<AnswerVariant> AnswerVariants;
    }
}