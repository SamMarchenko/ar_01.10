using System.Collections.Generic;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "ScenesSettingsPreset", menuName = "Data/AllScenesSettingsPreset")]
    public class ScenesSettingsPreset : ScriptableObject
    {
        [SerializeField] private List<SceneData> _sceneDatas;
    
        public List<SceneData> SceneDatas => _sceneDatas;
    
    }
}