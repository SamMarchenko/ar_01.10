using UnityEngine;

namespace Settings
{
    public class SceneSettings : MonoBehaviour
    {
        [SerializeField] private EducationObject _educationObject;
        public EducationObject EducationObject => _educationObject;
    }
}