using UnityEngine;
using UnityEngine.EventSystems;

namespace DefaultNamespace.Models
{
    public class Rotation : MonoBehaviour, IDragHandler
    {
        [SerializeField] private Camera _camera;
        public float speedRotateX = 5;
        public float speedRotateY = 5;

        public void OnDrag(PointerEventData eventData)
        {
            Debug.Log("OnDrag");
            if (Input.touchCount == 1 || Input.GetMouseButton(0) && !Input.GetKey(KeyCode.LeftControl))
            {
                Debug.Log("Rotate");
                Rotate();
            }
        }


        private void Rotate()
        {
            float rotX = Input.GetAxis("Mouse X") * speedRotateX * Mathf.Deg2Rad;
            float rotY = Input.GetAxis("Mouse Y") * speedRotateY * Mathf.Deg2Rad;

            
            if (Mathf.Abs(rotX) > Mathf.Abs(rotY))
                transform.RotateAroundLocal(transform.up, -rotX);
            else
            {
                var prev = transform.rotation;
                transform.RotateAround(_camera.transform.right, rotY);
                // if (Vector3.Dot(transform.up, _camera.transform.up) < 0.5f)
                //     transform.rotation = prev;
            }
        }
    }
}