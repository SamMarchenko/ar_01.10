using System.Collections;
using UnityEngine;


//todo: в сцене луны от этого скрипта избавился. В будущем уберем и из лунохода
public class ScreenshotComponent : MonoBehaviour
{
    public GameObject UI;
    private IEnumerator Screenshot()
    {
        yield return new WaitForEndOfFrame();
        Texture2D texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);

        texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        texture.Apply();

        string name = "AR_Library" + System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".png";

        NativeGallery.SaveImageToGallery(texture, "AR pictures", name);

        Destroy(texture);
        UI.SetActive(true);
    }

    public void TakeScreenshot()
    {
        UI.SetActive(false);
        StartCoroutine(Screenshot());
    }
}
