using UnityEngine;
using UnityEngine.SceneManagement;

public class Back : MonoBehaviour
{
    [SerializeField]
    private GameObject _panel;
    public void BackButtton()
    {
        SceneManager.LoadScene(0);
    }

    public void BackTest()
    {
        _panel.SetActive(false);
    }
}
