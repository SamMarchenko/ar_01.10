using System;
using System.Linq;
using TMPro;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Services.Core
{
    public class MainModelSceneService : IDisposable, IStartable, IUpdatable
    {
        private readonly MainUIView _mainUIView;
        private readonly EducationService _educationService;
        private readonly QuestionsTestService _questionsTestService;
        private readonly ScreenshotService _screenshotService;
        private readonly MusicService _musicService;
        private TextMeshProUGUI _tutorialMsg;

        public MainModelSceneService
            (
            MainUIView mainUIView,
            EducationService educationService,
            QuestionsTestService questionsTestService,
            ScreenshotService screenshotService,
            MusicService musicService
            )
        {
            _mainUIView = mainUIView;
            _educationService = educationService;
            _questionsTestService = questionsTestService;
            _screenshotService = screenshotService;
            _musicService = musicService;
        }

        public void Start()
        {
            InitView();
            Subscribe();
        }

        public void Update()
        {
            if (_mainUIView.gameObject.activeSelf == false)
                return;

            if (Input.touchCount > 0)
            {
                _tutorialMsg.gameObject.SetActive(false);
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    var ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                    if (Physics.Raycast(ray))
                    {
                        SceneManager.LoadScene(1);
                    }
                }
            }
            else
            {
                _tutorialMsg.gameObject.SetActive(true);
            }
        }

        private void InitView()
        {
            var musicSettings = GetCurrentMusicSettings();
            _musicService.SetDefaultSound(musicSettings);
            ChangeSoundBtnSprite(musicSettings);
            _tutorialMsg = _mainUIView.TutorialMsgText;
        }

        private bool GetCurrentMusicSettings()
        {
            bool musicEnabled;
            if (!PlayerPrefs.HasKey(SavesStrings.Music))
            {
                musicEnabled = true;
                return musicEnabled;
            }

            var i = PlayerPrefs.GetInt(SavesStrings.Music);

            musicEnabled = i == 1;
            return musicEnabled;
        }

        private void ShowMainSceneUI() =>
            _mainUIView.Show();

        private void OnSound()
        {
            var musicSettings = GetCurrentMusicSettings();
            _musicService.SwitchSound(musicSettings);
            ChangeSoundBtnSprite(!musicSettings);
        }

        private void ChangeSoundBtnSprite(bool musicSettings)
        {
            var sprite = _mainUIView.SoundButtonViews
                .FirstOrDefault(s => s.IsSoundOn == musicSettings)?.ViewSprite;
            if (sprite == null)
            {
                Debug.LogError("Для кнопки звука не настроены спрайты");
            }

            _mainUIView.SoundBtn.image.sprite = sprite;
        }

        private void OnScreenshot()
        {
            _mainUIView.Hide();
            _screenshotService.TakeScreenshot();
        }

        private void OnEducation()
        {
            _mainUIView.Hide();
            _educationService.OpenEducationView();
        }

        public void Dispose() =>
            Unsubscribe();

        private void Subscribe()
        {
            _educationService.OnEducationQuit += ShowMainSceneUI;
            _screenshotService.OnScreenshotMade += ShowMainSceneUI;
            _questionsTestService.OnNeedOpenMainModelScene += ShowMainSceneUI;

            _mainUIView.EducationBtn.onClick.AddListener(OnEducation);
            _mainUIView.ScreenshotBtn.onClick.AddListener(OnScreenshot);
            _mainUIView.SoundBtn.onClick.AddListener(OnSound);
        }

        private void Unsubscribe()
        {
            _educationService.OnEducationQuit -= ShowMainSceneUI;
            _screenshotService.OnScreenshotMade -= ShowMainSceneUI;
            _questionsTestService.OnNeedOpenMainModelScene -= ShowMainSceneUI;

            _mainUIView.EducationBtn.onClick.RemoveAllListeners();
            _mainUIView.ScreenshotBtn.onClick.RemoveAllListeners();
            _mainUIView.SoundBtn.onClick.RemoveAllListeners();
        }
    }
}