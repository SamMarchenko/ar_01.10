namespace Services.Core
{
    public interface IUpdatable
    {
        public void Update();
    }
}