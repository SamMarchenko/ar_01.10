using System;
using System.Collections;
using System.Collections.Generic;
using Data;
using UI;
using UnityEngine;
using UnityEngine.Events;

namespace Services.Core
{
    public class QuestionsTestService : INeedSceneData
    {
        private const int DefaultQuestionNumber = -1;
        private readonly QuestionsWindowView _questionsWindowView;
        private readonly ICoroutineRunner _coroutineRunner;
        private SceneData _sceneData;
        private int _currentQuestionNumber;
        private float _currentMinimalFontSize = float.MaxValue;
        private TestQuestion _currentQuestionData;
        private List<QuestionsWindowView.VariantBtnView> _activeBtns = new List<QuestionsWindowView.VariantBtnView>();
        private readonly FinishTestWindowView _finishTestWindowView;
        private TestProgressStatistic _currentTestStats;
        private bool _isLastQuestion;

        public Action OnNeedOpenMainModelScene;

        public QuestionsTestService(QuestionsWindowView questionsWindowView, ICoroutineRunner coroutineRunner)
        {
            _questionsWindowView = questionsWindowView;
            _finishTestWindowView = questionsWindowView.FinishTestWindowView;
            _finishTestWindowView.Hide();
            _coroutineRunner = coroutineRunner;
            _currentTestStats = new TestProgressStatistic();
        }

        public void OpenTestQuestionsView()
        {
            _currentQuestionNumber = DefaultQuestionNumber;
            _currentTestStats.AllQuestionsNumber = _sceneData.EduTestData.TestQuestions.Count;
            GetActualQuestion();
            InitQuestionWindowView();
            _questionsWindowView.Show();
        }

        public void CloseTestQuestionsView()
        {
            _currentMinimalFontSize = float.MaxValue;
            _questionsWindowView.ResetAllBtnsFontSize();
            _isLastQuestion = false;
            _currentTestStats.Clear();
            _currentQuestionNumber = DefaultQuestionNumber;
            UnsubscribeAllBtns();
            _questionsWindowView.Hide();
            _finishTestWindowView.Hide();
        }

        public void SetSceneData(SceneData data) =>
            _sceneData = data;

        private void InitQuestionWindowView()
        {
            SetQuestionText(_currentQuestionData.QuestionText);
            InitVariantButtons();
            
            SubscribeNextQuestionBtn();
        }


        private void InitVariantButtons()
        {
            for (int i = 0; i < _currentQuestionData.AnswerVariants.Count; i++)
            {
                var btnView = _questionsWindowView.VariantsBtns[i];
                var variantData = _currentQuestionData.AnswerVariants[i];

                btnView.Btn.interactable = true;
                btnView.BtnText.text = variantData.VariantText;
                btnView.IsRightAnswer = variantData.IsRightAnswer;
                btnView.BtnImage.sprite = _questionsWindowView.AnswerSprites[2];

                SubscribeVariantBtn(btnView);

                _activeBtns.Add(btnView);
            }

            _coroutineRunner.StartCoroutine(UpdateFontSize());
        }


        private void FindMinFontSize(float fontSize)
        {
            if (_currentMinimalFontSize > fontSize)
                _currentMinimalFontSize = fontSize;
        }

        private void SetQuestionText(string text) =>
            _questionsWindowView.QuestionText.text = text;


        private void ResetQuestionWindowView()
        {
            SetQuestionText(string.Empty);

            _currentMinimalFontSize = float.MaxValue;

            foreach (var variantBtnView in _activeBtns)
            {
                variantBtnView.BtnText.text = string.Empty;
                variantBtnView.IsRightAnswer = false;
                variantBtnView.Btn.gameObject.SetActive(false);
            }
        }

        #region ButtonsCallBacks

        private void ModelSceneBtnClicked()
        {
            CloseTestQuestionsView();
            OnNeedOpenMainModelScene?.Invoke();
        }


        private void MainMenuBtnClicked()
        {
            CloseTestQuestionsView();
            OnNeedOpenMainModelScene?.Invoke();
        }

        private void OnVariantBtnClicked(QuestionsWindowView.VariantBtnView btnView)
        {
            foreach (var view in _activeBtns)
            {
                view.Btn.interactable = false;
            }

            if (btnView.IsRightAnswer)
            {
                _currentTestStats.RightAnswers++;
                Debug.Log($"Правильных ответов: {_currentTestStats.RightAnswers}");
                btnView.BtnImage.sprite = _questionsWindowView.AnswerSprites[1];
            }
            else
            {
                _currentTestStats.WrongAnswers++;
                Debug.Log($"Неправильных ответов: {_currentTestStats.WrongAnswers}");
                btnView.BtnImage.sprite = _questionsWindowView.AnswerSprites[0];
                var btnImage = _activeBtns.Find(b => b.IsRightAnswer).BtnImage;
                btnImage.sprite = _questionsWindowView.AnswerSprites[1];
            }

            _currentTestStats.PassedQuestions++;
            Debug.Log($"Пройдено вопросов: {_currentTestStats.PassedQuestions}");

            if (_isLastQuestion)
            {
                _coroutineRunner.StartCoroutine(ShowFinishWindow());
                return;
            }

            InitNextQuestionBtn(true);
        }

        private void OnNextQuestionBtnClicked()
        {
            //todo: логика клика на кнопку следующего вопроса

            UnsubscribeNextQuestionBtn();
            ResetQuestionWindowView();
            UnsubscribeVariantBtns();
            _activeBtns.Clear();
            GetActualQuestion();
            InitQuestionWindowView();
        }

        #endregion

        #region Coroutines

        private IEnumerator ShowFinishWindow()
        {
            _finishTestWindowView.ResultTex.text = $"Правильные ответы: {_currentTestStats.RightAnswers}";
            _finishTestWindowView.MainMenuBtn.onClick.AddListener(MainMenuBtnClicked);
            _finishTestWindowView.ModelSceneBtn.onClick.AddListener(ModelSceneBtnClicked);
            yield return new WaitForSeconds(_finishTestWindowView.DelayBeforeShow);

            _finishTestWindowView.Show();
        }

        IEnumerator UpdateFontSize()
        {
            foreach (var variantBtnView in _activeBtns)
            {
                variantBtnView.Btn.gameObject.SetActive(true);
                variantBtnView.BtnImage.enabled = false;
            }

            yield return null;

            foreach (var variantBtnView in _activeBtns)
                FindMinFontSize(variantBtnView.BtnText.fontSize);

            foreach (var variantBtnView in _activeBtns)
            {
                variantBtnView.BtnText.fontSizeMax = _currentMinimalFontSize;
                variantBtnView.BtnImage.enabled = true;
            }
        }

        #endregion


        private void GetActualQuestion()
        {
            _currentQuestionNumber++;

            if (_sceneData.EduTestData.TestQuestions.Count - 1 == _currentQuestionNumber)
            {
                Debug.Log("Это последний вопрос.");
                _currentQuestionData = _sceneData.EduTestData.TestQuestions[_currentQuestionNumber];

                _isLastQuestion = true;

                _questionsWindowView.NextQuestionButtonView.Btn.gameObject.SetActive(false);
                return;
            }

            _isLastQuestion = false;
            _questionsWindowView.NextQuestionButtonView.Btn.gameObject.SetActive(true);
            _currentQuestionData = _sceneData.EduTestData.TestQuestions[_currentQuestionNumber];
            InitNextQuestionBtn(isEnableBtn: false);
        }

        private void InitNextQuestionBtn(bool isEnableBtn)
        {
            var buttonView = _questionsWindowView.NextQuestionButtonView;

            if (!isEnableBtn)
            {
                buttonView.Btn.interactable = false;
                buttonView.BtnImage.sprite = buttonView.BtnViewSprites[0];
                buttonView.BtnText.color = buttonView.DisableBtnTextColor;
            }
            else
            {
                buttonView.Btn.interactable = true;
                buttonView.BtnImage.sprite = buttonView.BtnViewSprites[1];
                buttonView.BtnText.color = Color.white;
            }
        }

        #region Subscribe

        public void AddCallbackToBackButton(UnityAction action)
        {
            _questionsWindowView.BackBtn.onClick.AddListener(action);
        }

        private void SubscribeNextQuestionBtn() =>
            _questionsWindowView.NextQuestionButtonView.Btn.onClick.AddListener(OnNextQuestionBtnClicked);

        private void SubscribeVariantBtn(QuestionsWindowView.VariantBtnView btnView) =>
            btnView.Btn.onClick.AddListener(() => OnVariantBtnClicked(btnView));

        private void UnsubscribeVariantBtns()
        {
            foreach (var subscribedBtn in _activeBtns)
                subscribedBtn.Btn.onClick.RemoveAllListeners();
        }

        private void UnsubscribeAllBtns()
        {
            UnsubscribeVariantBtns();
            UnsubscribeNextQuestionBtn();
        }

        private void UnsubscribeNextQuestionBtn() =>
            _questionsWindowView.NextQuestionButtonView.Btn.onClick.RemoveAllListeners();

        #endregion


        class TestProgressStatistic
        {
            public int RightAnswers;
            public int WrongAnswers;
            public int PassedQuestions;
            public int AllQuestionsNumber;

            public void Clear()
            {
                RightAnswers = 0;
                WrongAnswers = 0;
                PassedQuestions = 0;
                AllQuestionsNumber = 0;
            }
        }
    }
}