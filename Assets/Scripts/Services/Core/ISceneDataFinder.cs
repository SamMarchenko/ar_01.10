using Data;
using Settings;

namespace Services.Core
{
    public interface ISceneDataFinder
    {
        void FindSceneData(ScenesSettingsPreset settingsPreset);
    }
}