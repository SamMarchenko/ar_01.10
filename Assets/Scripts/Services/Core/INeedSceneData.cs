using Data;

namespace Services.Core
{
    public interface INeedSceneData
    {
        void SetSceneData(SceneData data);
    }
}