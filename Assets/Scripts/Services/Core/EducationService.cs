using System;
using Data;
using UI;

namespace Services.Core
{
    public class EducationService : INeedSceneData, IStartable
    {
        public Action OnEducationQuit;

        private readonly EducationContentView _educationContentView;
        private readonly QuestionsTestService _questionsTestService;
        private SceneData _sceneData;

        public EducationService(EducationContentView educationContentView, QuestionsTestService questionsTestService)
        {
            _educationContentView = educationContentView;
            _questionsTestService = questionsTestService;
        }

        public void Start()
        {
            InitView();
            Subscribe();
        }
        
        private void InitView()
        {
            var chapter = _sceneData.EduTestData.ChapterText;
            var header = _sceneData.EduTestData.HeaderText;
            var eduText = _sceneData.EduTestData.EducationText;

            _educationContentView.SetAllText(chapter, header, eduText);
            _educationContentView.SetBackgroundImage(_sceneData.EduBackgroundImageSprite);
        }

        public void OpenEducationView()
        {
            _educationContentView.Show();
        }

        private void Subscribe()
        {
            _educationContentView.AddCallbackToBackButton(() => OnBackButtonClicked());
            _educationContentView.StartTestBtn.onClick.AddListener(OnTestStartClicked);
            _educationContentView.SpeakerBtn.onClick.AddListener(OnSpeakerBtnClicked);

            _questionsTestService.AddCallbackToBackButton(() => ReturnInEduTextFromQuestions());
        }

        private void ReturnInEduTextFromQuestions()
        {
            _educationContentView.Show();
            _questionsTestService.CloseTestQuestionsView();
        }

        private void OnSpeakerBtnClicked()
        {
            
        }

        private void OnTestStartClicked()
        {
            _questionsTestService.OpenTestQuestionsView();
            _educationContentView.Hide();
        }

        private void OnBackButtonClicked()
        {
            _educationContentView.Hide();
            OnEducationQuit?.Invoke();
        }

        public void SetSceneData(SceneData data)
        {
            _sceneData = data;
        }
    }
}