using UnityEngine;

namespace Services.Core
{
    public class MusicService
    {
        private readonly AudioSource _audioSource;
        

        public MusicService(AudioSource audioSource)
        {
            _audioSource = audioSource;
        }

        public void SwitchSound(bool currentSetting)
        {
            var newSetting = !currentSetting;
            _audioSource.enabled = newSetting;
            Debug.Log($"Музыка {_audioSource.enabled}");
            
            int save = newSetting ? 1 : 0;
            PlayerPrefs.SetInt(SavesStrings.Music, save);
            
        }
        public void SetDefaultSound(bool currentSetting)
        {
            _audioSource.enabled = currentSetting;
            Debug.Log($"Музыка {_audioSource.enabled}");
            
            int save = currentSetting ? 1 : 0;
            PlayerPrefs.SetInt(SavesStrings.Music, save);
            
        }
    }
}