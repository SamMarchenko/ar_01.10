using System;
using System.Collections;
using UnityEngine;

namespace Services.Core
{
    public class ScreenshotService
    {
        private readonly ICoroutineRunner _coroutineRunner;

        public Action OnScreenshotMade;

        public ScreenshotService(ICoroutineRunner coroutineRunner)
        {
            _coroutineRunner = coroutineRunner;
        }
        
        public void TakeScreenshot() => 
            _coroutineRunner.StartCoroutine(Screenshot());

        private IEnumerator Screenshot()
        {
            Debug.Log("Начал делать скриншот");
            yield return new WaitForEndOfFrame();
            Texture2D texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);

            texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
            texture.Apply();

            string name = "AR_Library" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".png";

            NativeGallery.SaveImageToGallery(texture, "AR pictures", name);

           GameObject.Destroy(texture);

           OnScreenshotMade?.Invoke();
           Debug.Log("Закончил делать скриншот");
        }
    }
}