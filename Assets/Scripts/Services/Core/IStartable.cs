namespace Services.Core
{
    public interface IStartable
    {
        public void Start();
    }
}