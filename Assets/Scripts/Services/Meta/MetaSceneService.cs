using TemporaryScripts;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Zenject;

namespace Services.Meta
{
    public class MetaSceneService : IInitializable
    {
        private readonly MetaSceneView _metaSceneView;
        private bool _isLoading;

        public MetaSceneService(MetaSceneView metaSceneView)
        {
            _metaSceneView = metaSceneView;
        }

        public void Initialize()
        {
            Subscribe();
        }

        private void Subscribe()
        {
            foreach (var objSceneBtnView in _metaSceneView.ObjSceneBtnViews)
                objSceneBtnView.AddCallbackToLoadArSceneButton(() => OnStartBtnClicked(objSceneBtnView));
        }

       

        private UnityAction OnStartBtnClicked(MetaSceneView.ObjSceneBtnView objSceneBtnView)
        {
            switch (objSceneBtnView.EducationObject)
            {
                case EducationObject.Moon:
                    LoadCoreScene(SavesStrings.MoonObj);
                    break;
                case EducationObject.MoonRover:
                    LoadCoreScene(SavesStrings.MoonRoverObj);
                    break;
            }


            return null;
        }

        private void LoadCoreScene(string educationObjectName)
        {
            if (_isLoading)
                return;
            PlayerPrefs.SetString(SavesStrings.EducationObject, educationObjectName);
            SceneManager.LoadScene(1);
            _isLoading = true;
        }
    }
}