using UnityEngine;

public class TestManager : MonoBehaviour
{
    public GameObject firstQuestion;
    public GameObject secondQuestion;
    public GameObject thirdQuestion;
    public GameObject finish;
    private InformationComponent _info;

    private void Start()
    {
        _info = FindObjectOfType<InformationComponent>();
    }

    public void StartTest()
    {
        firstQuestion.SetActive(true);
    }

    public void SecondQuestion()
    {
        secondQuestion.SetActive(true);
    }

    public void ThirdQuestion()
    {
        thirdQuestion.SetActive(true);
    }

    public void FinishTest()
    {
        finish.SetActive(true);
    }

    public void BackToModel()
    {
        finish.SetActive(false);
        thirdQuestion.SetActive(false);
        secondQuestion.SetActive(false);
        firstQuestion.SetActive(false);
        _info.CloseInfo();
    }
}
