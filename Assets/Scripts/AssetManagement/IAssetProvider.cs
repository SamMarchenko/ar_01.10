using UnityEngine;

namespace AssetManagement
{
    public interface IAssetProvider
    {
        GameObject Instantiate(string path);
    }
}