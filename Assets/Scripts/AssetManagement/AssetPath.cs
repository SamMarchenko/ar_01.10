namespace AssetManagement
{
    public static class AssetPath
    {
        public const string MoonPath = "ArObjPrefabs/Space/Moon";
        public const string MoonRoverPath = "ArObjPrefabs/Space/MoonRover";
    }
}