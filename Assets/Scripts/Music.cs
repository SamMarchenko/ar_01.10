using UnityEngine;
using UnityEngine.UI;

//todo: в сцене луны от этого скрипта избавился. В будущем уберем и из лунохода
public class Music : MonoBehaviour
{
    public Sprite _onMusic;
    public Sprite _offMusic;

    public Image _musicButton;
    public bool _isOn;
    public AudioSource _audioSource;

    private void Start()
    {
        _isOn = false;
    }

    private void Update()
    {
        if(PlayerPrefs.GetInt("music")==0)
        {
            _musicButton.GetComponent<Image>().sprite = _onMusic;
            _audioSource.enabled = true;
            _isOn = true;
        }
        else if(PlayerPrefs.GetInt("music")==1)
        {
            _musicButton.GetComponent<Image>().sprite = _offMusic;
            _audioSource.enabled = false;
            _isOn = false;
        }
    }

    public void OffSound()
    {
        if(!_isOn)
        {
            PlayerPrefs.SetInt("music", 0);
        }
        else if(_isOn)
        {
            PlayerPrefs.SetInt("music", 1);
        }
    }
}
